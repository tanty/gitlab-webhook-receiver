FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY gitlab-webhook-receiver.py .
COPY valve-ci-pipeline-executor/ valve-ci-pipeline-executor/
COPY entrypoint.sh .

CMD [ "./entrypoint.sh" ]
