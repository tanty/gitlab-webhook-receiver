# coding=utf-8
#
# Copyright © 2021 Valve Corporation.
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

""" Valve-CI module for GitLab Webhook Receiver """

import gitlab
import logging
import os
import re
import sys
import yaml

from datetime import datetime, timedelta
from os import path
from urllib.parse import urlparse

class Executor:
    config_file = path.join(
        os.getenv('VALVE_CI_PIPELINE_EXECUTOR_CONFIG_PATH') or "",
        "./config.yaml")

    def __init__(self, payload, receiver):
        with open(Executor.config_file, 'r') as stream:
            self.yaml = yaml.safe_load(stream)
        self.payload = payload
        self.receiver = receiver
        self.receiver = receiver
        self.project_name = None # Lazy load
        self.project = None # Lazy load

    def do_token_mgmt(self, gitlab_token_header):
        if self.payload['object_kind'] == 'pipeline':
            self.project_name = self.payload['project']['web_url']
            deal_with_payload = self.deal_with_pipeline
        elif self.payload['object_kind'] == 'build':
            self.project_name = self.payload['repository']['homepage']
            deal_with_payload = self.deal_with_job
        else:
            logging.info("Not a pipeline nor job. Skipping ...")
            self.receiver.send_response(200, "OK")
            return

        self.project = self.yaml[self.project_name]
        # Check if the gitlab token is valid

        if gitlab_token_header == self.project['gitlab_token']:
            # Answer as quickly as possible
            self.receiver.send_response(200, "OK")
            deal_with_payload()
        else:
            self.receiver.send_response(401, "Gitlab Token not authorized")
            logging.error("Not authorized, Gitlab_Token not authorized")

    def deal_with_pipeline(self):
        if (self.payload['object_attributes']['source'] != 'push' or
            self.payload['object_attributes']['status'] != 'manual'):
            logging.info("Not a pipeline waiting for manual action. "
                         "Skipping ...")
            return

        ref_pattern = re.compile(self.project['ref_pattern'])
        if not ref_pattern.match(self.payload['object_attributes']['ref']):
            logging.info("No ref_pattern matching. Skipping ...")
            return

        created_at = datetime.strptime(
            self.payload['object_attributes']['created_at'],
            '%Y-%m-%d %H:%M:%S %Z')
        delta = datetime.utcnow() - created_at
        if delta > timedelta(seconds=10):
            logging.info("Old pipeline. Skipping ...")
            return

        sentinel_job = self.project['sentinel_job']
        has_sentinel_job = False
        for build in self.payload['builds']:
            if build['name'] == sentinel_job:
                has_sentinel_job = True
                break

        if not has_sentinel_job:
            logging.info("No sentinel job. Skipping ...")
            return

        logging.info("Dealing with this pipeline.")
        parsed_uri = urlparse(self.project_name)
        gl_server = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        gl = gitlab.Gitlab(gl_server,
                           private_token=self.project['gitlab_access_token'])
        # gl = gitlab.Gitlab(gl_server)
        project_id = self.payload['project']['id']
        project = gl.projects.get(project_id)

        play_jobs = self.project['play_jobs']
        cancel_jobs = self.project['cancel_jobs']
        for build in self.payload['builds']:
            if (build['name'] in play_jobs and
                build['started_at'] is None):
                logging.info("Playing %s.", build['name'])
                job = project.jobs.get(build['id'], lazy=True)
                job.play()
            if build['name'] in cancel_jobs:
                logging.info("Canceling %s.", build['name'])
                job = project.jobs.get(build['id'], lazy=True)
                job.cancel()

    def deal_with_job(self):
        # Use the sentinel job to force playing other jobs
        if (self.payload['build_name'] != self.project['sentinel_job'] or
            self.payload['build_status'] != 'running'):
            # logging.info("Not the sentinel job we are looking for. "
            #               "Skipping ...")
            return

        parsed_uri = urlparse(self.project_name)
        gl_server = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
        gl = gitlab.Gitlab(gl_server,
                           private_token=self.project['gitlab_access_token'])
        # gl = gitlab.Gitlab(gl_server)
        project_id = self.payload['project_id']
        project = gl.projects.get(project_id)
        pipeline_id = self.payload['pipeline_id']
        pipeline = project.pipelines.get(pipeline_id)

        ref_pattern = re.compile(self.project['ref_pattern'])
        if not ref_pattern.match(pipeline.ref):
            logging.info("No ref_pattern matching. Skipping ...")
            return

        play_jobs = self.project['sentinel_play_jobs']
        for pipeline_job in pipeline.jobs.list(scope='manual'):
            if (pipeline_job.name in play_jobs and
                pipeline_job.started_at is None):
                logging.info("Playing %s.", pipeline_job.name)
                job = project.jobs.get(pipeline_job.id, lazy=True)
                job.play()


def main(gitlab_token_header, json_params, receiver, receiver_args):
    """Deal with request."""
    executor = Executor(json_params, receiver)
    executor.do_token_mgmt(gitlab_token_header)
